# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_hash = {}

  str.split.each { |word| word_hash[word] = word.length }

  word_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = newer[k]
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  frequencies = Hash.new(0)

  word.split("").each { |letter| frequencies[letter] += 1 }

  frequencies
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  count = Hash.new(0)

  arr.each { |el| count[el] += 1 }

  count.keys
end


# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parities = Hash.new(0)

  numbers.each do |number|
    if number.odd?
      parities[:odd] += 1
    else
      parities[:even] += 1
    end
  end

  parities
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  most_common_vowel = two_most_common_vowels(string)[0]
  second_most_common_vowel = two_most_common_vowels(string)[1]

  sort_vowels_by_alphabetical = [second_most_common_vowel.first, most_common_vowel.first].sort

  if second_most_common_vowel.first == most_common_vowel.first
    sort_vowels_by_alphabetical.first
  else
    most_common_vowel.first
  end
end

def two_most_common_vowels(string)
  vowels_by_frequency = count_vowels(string).sort_by { |k, v| v }

  [vowels_by_frequency.last, vowels_by_frequency[-2]]
end

def count_vowels(string)
  vowels = ["a", "e", "i", "o", "u"]
  count = Hash.new(0)

  string.split("").each { |ch| count[ch] += 1 if vowels.include?(ch) }

  count
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  winter_students = birthdays_in_second_half_of_year(students)

  find_all_combinations(winter_students.keys)
end

def birthdays_in_second_half_of_year(students)
  students.select { |k, v| v > 6 }
end

def find_all_combinations(students)
  combos = []

  students.each_with_index do |student, idx|
    idx2 = idx

    while idx2 < students.length - 1
      idx2 += 1
      otherStudent = students[idx2]
      combos << [student, otherStudent]
    end
  end

  combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  sorted = countSpecies(specimens).sort_by {|k, v| v}

  number_of_species = sorted.length
  smallest_population_size = sorted.first[1]
  largest_population_size = sorted.last[1]

  number_of_species**2 * smallest_population_size / largest_population_size
end

def countSpecies(specimens)
  tally = Hash.new(0)

  specimens.each do |specimen|
    tally[specimen] +=1
  end

  tally
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true

def can_tweak_sign?(normal_sign, vandalized_sign)
  word_bank = character_count(normal_sign.downcase)

  vandalized_sign.downcase.split(" ").join.split("").each do |ch|
    if word_bank[ch] != nil && word_bank[ch] > 0
      word_bank[ch] -=1
    else
      return false
    end
  end

  true
end

def character_count(str)
  count = Hash.new(0)

  str.split(' ').join.split('').each { |ch| count[ch] +=1 if /[A-Za-z]/.match(ch) }

  count
end
